from http.server import HTTPServer, BaseHTTPRequestHandler

class MyHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(300)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        message = "Hello, World!"
        self.wfile.write(message.encode())

if __name__ == "__main__":
    server_address = ("", 8000)
    httpd = HTTPServer(server_address, MyHandler)
    print("Starting web server...")
    httpd.serve_forever()
